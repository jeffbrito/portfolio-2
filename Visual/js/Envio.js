$(document).ready(function(){  
    $('.form').submit(function(e){
        e.preventDefault();
        var form_data = $('form').serialize();
        $.ajax({
            url:'../Controle/Enviar.php',
            type: 'POST',
            data: form_data,
            beforeSend: function(){
                $('#loader').html("Enviando email <img class='img-fluid' src='imagens/spinner.gif' width='50px' height='100px'/>")
            }
        
        }).done(function(textStatus, jqXHR){
            $('form')[0].reset();
            $('#loader').html('')
            $('#modalSucess').modal('show')
        })
        .fail(function(jqXHR, textStatus, errorThrown){
            $('#modalFail').modal('show')
        });

    });
});