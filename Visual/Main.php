<?php
	echo"
		<!Doctype html>
		<html lang='pt-br'> 
			<head>
				<meta charset='utf-8'/>
				<link rel='stylesheet' type='text/css' href='css/bootstrap-4.1.3-dist/css/bootstrap.css' />
				<link rel='stylesheet' type='text/css' href='css/Main.css' />
				<meta name='viewport' content='width=device-width, initial-scale=1'>
			</head>
			<body>
					<header>
								<nav class='navbar navbar-expand-lg navbar-dark' style='background-color:#000;'>
								<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarTogglerDemo01' aria-controls='navbarTogglerDemo01' aria-expanded='' aria-label='Alterna navegação'>
								<span class='navbar-toggler-icon'></span>
							  </button>
							  <div class='collapse navbar-collapse' id='navbarTogglerDemo01'>
											<a class='navbar-brand' href='#'>JO</a>
											<ul class='navbar-nav mr-auto'>
												<li class='nav-item'>
													<a class='nav-link' href='#'>Início</a>
												</li>
											
												<li class='nav-item'>
													<a class='nav-link' href='#sobre'>Sobre</a>
												</li>
												<li class='nav-item'>
													<a class='nav-link' href='#contate-me'>Contate-me</a>
												</li>
											</ul>
										</div>
								</nav>

					</header>
					
						
							<div class='container-fluid parallax'/>
									<h2>Jefferson Oliveira</h2>
									<p>Desenvolvedor full-stack</p>
							</div>	
							
							<div class='container-fluid'>
									<ul class='nav redess justify-content-center'>
										<li class='nav-item'>
											<a class='nav-link' href='https://github.com/jeff-gif'><img src='imagens/github.svg' width='40' height='40'/></a>
										</li>

										<li class='nav-item'>
											<a class='nav-link' href='https://gitlab.com/jeffbrito'><img src='imagens/gitlab.svg' width='40' height='40'/></a>
										</li>

										<li class='nav-item'>
											<a class='nav-link' href='https://www.linkedin.com/in/jefferson-oliveira-4103131ba/'><img src='imagens/linkedin.svg' width='40' height='40'/></a>
										</li>
									</ul>
							</div>

							<div id='sobre' class='sobre container-fluid'>
								<div class='row no-gutters'>
									<div class='col-sm'>
										<h1 class='title'>Um pouco sobre mim</h1>

										<p>Desenvolvedor Full Stack em início de carreira. Técnico em informática pela EEEP Paulo Petrola.
										Gosto de tecnologia desde pequeno e com a programação não
										foi diferente.
										Comecei pela linguagem
										de programação C, depois conheci outras linguagens, e assim se apaixonando ainda mais pela área. </p>									
									</div>
								</div>
							</div>

							<div class='container-fluid imagemJeff'>
								<div class='row no-gutters'>
									<div class='col-sm'>
										<h5 class='textao'>Nome</h5>
										<p class='textinho'>Jefferson Brito de Oliveira</p> 
										<h5 class='textao'>Nascimento</h5>
										<p class='textinho'>04 de Agosto de 2002</p>
									</div>
									<div class='col-sm'>
										<img src='imagens/Jeff.jpeg' class='rounded img-fluid' style='width:160px;height:160px;'  alt='Jefferson' />
									</div>
									<div class='col-sm'>
										<h5 class='textao'>Website</h5>
										<p class='textinho'>jbrito.ntectreinamentos.com.br/portfolio</p>
										<h5 class='textao'>Email</h5>
										<p class='textinho'>jefferson.dev.alu@gmail.com</p>
									</div>
								</div>

							</div>
											
								<div class='container-fluid'>					
									<h3>Skills</h3>
									<p>Front-end</p>
									<div class='progress'>
										<div class='progress-bar' role='progressbar' style='width:40%;' aria-valuenow='40' area-valuemin='0' area-valuemax='100'  >40%</div>
									</div>

									<p>Back-end</p>
									<div class='progress'>
										<div class='progress-bar' role='progressbar' style='width:35%;' aria-valuenow='35' area-valuemin='0' area-valuemax='100'  >35%</div>
									</div>

									<p>Desenvolvimento web</p>
									<div class='progress'>
										<div class='progress-bar' role='progressbar' style='width:50%;' aria-valuenow='50' area-valuemin='0' area-valuemax='100'  >50%</div>
									</div>

									<p>Desktop</p>
									<div class='progress'>
										<div class='progress-bar' role='progressbar' style='width:50%;' aria-valuenow='50' area-valuemin='0' area-valuemax='100'  >50%</div>
									</div>

									<p>Design gráfico</p>
									<div class='progress'>
										<div class='progress-bar' role='progressbar' style='width:20%;' aria-valuenow='20' area-valuemin='0' area-valuemax='100'  >20%</div>
									</div>

								</div>

								<div class='container-fluid quali'>
										<h2>Qualificações</h2>
										<ul>
											<li>Linguagens de programação(C,C++,Python,Java,php)</li>
											<li>Programação Orientada a objetos(C++,Java,Python,php)</li>
											<li>Desenvolvimento Web(JavaScript,Html,Css,JavaWeb)</li>
											<li>Frameworks(Bootstrap e Bulma)</li>
											<li>Desenvolvimento Desktop(Python/Glade,C++/Qt)</li>
											<li>Banco de Dados(Mysql/MariaDb)</li>
											<li>Inglês Técnico</li>

										</ul>
								</div>

								<div class='concurri container-fluid'>
									<a href='Documentos/CurriculumJO.pdf' type='button' class='btn btn-primary' download>Curriculo Digital</a>
								</div>
							";
							/*modals*/
							echo"
								<!-- Modal de Sucesso -->
									<div class='modal fade' id='modalSucess' tabindex='-1' role='dialog' aria-labelledby='titulosucesso' aria-hidden='true' >
										<div class='modal-dialog' role='document'>
											<div class='modal-content'>
												<div class='modal-header'>
													<h5 class='modal-titler' id='titulosucesso'>
														Mensagem
													</h5> 
													<button type='button' class='close' data-dismiss='modal' aria-label='Fechar'>
														<span aria-hidden='true'>&times</span>
													</button>
												</div>
												<div class='modal-body'>
														Seu email foi enviado com sucesso!!!
												</div>
												<div class='modal-footer'>
													<button type='button' class='btn btn-secondary' data-dismiss='modal'>
														Fechar
													</button>
												</div>
											</div>
										</div>
									</div>

									<!-- Modal de erro -->
									<div class='modal fade' id='modalFail' tabindex='-1' role='dialog' aria-labelledby='tituloFail' aria-hidden='true' >
										<div class='modal-dialog' role='document'>
											<div class='modal-content'>
												<div class='modal-header'>
													<h5 class='modal-titler' id='tituloFail'>
														Mensagem
													</h5> 
													<button type='button' class='close' data-dismiss='modal' aria-label='Fechar'>
														<span aria-hidden='true'>&times</span>
													</button>
												</div>
												<div class='modal-body'>
														Erro ao enviar seu email, verifique se preencheu corretamente os campos e envie novamente.
												</div>
												<div class='modal-footer'>
													<button type='button' class='btn btn-secondary' data-dismiss='modal'>
														Fechar
													</button>
												</div>
											</div>
										</div>
									</div>
							
							";
							echo"
							<div id='cont' class='container-fluid form contate-me fundo'>
							<form id='form' class='form cax' action='../Controle/Enviar.php' method='POST'>
								
								<h2 class='cont'>Contate-me</h2>

								<div class='form-group'>
									<label>Nome</label>
										<input type='text' id='nome' name='nome' placeholder='Insira seu nome aqui' />
								</div>

								<div class='form-group'>
									<label>Email</label>
										<input type='email' id='email' name='email' placeholder='Insira seu email' />
								</div>

								<div class='form-group'>
									<label>Assunto</label>
										<input type='text' id='assunto' name='assunto' placeholder='Insira seu assunto' />
								</div>
								<div class='form-group'>
									<label>Mensagem</label>
										<textarea  id='mensagem' name='mensagem' placeholder='Insira o que você quer falar comigo'></textarea>
								</div>

								<div class='form-group'>
									<button id='submit' type='submit' class='btn btn-primary'>Enviar</button>
								</div>
								
								<div id='loader'></div>
							 </form>								
						</div>


							";
							echo"

								<div class='container-fluid'>
									<h3> Projetos </h3>
									<div class='row'>
										<div class='col-sm'>
											<div class='card ca' style='width: 18rem;'>
													<img class='card-img-top img-fluid' src='imagens/bannerT.svg' alt='Imagem de capa do card'>
												<div class='card-body'>
													<h5 class='card-title'>Bucho de Bode</h5>
													<p class='card-text'>Um sistema feito para um restaurante fictício. Projeto feito com html5,css3,php e js. Com a ajuda do framework materialize, foi feito em grupo, sendo eu o responsável por um pouco do front-end e tendo mais participação no back-end, como no cadastro do cliente.
													</p>
													<a href='http://jbrito.ntectreinamentos.com.br/bucho-de-bode/' class='btn btn-primary'>Visitar</a>
												</div>
											</div>
										</div>

										<div class='col-sm'>
											<div class='card ca' style='width: 18rem;'>
													<img class='card-img-top img-fluid' src='imagens/logo.png' alt='Imagem de capa do card'>
												<div class='card-body'>
													<h5 class='card-title'>Simplify</h5>
													<p class='card-text'>Projeto feito em grupo, buscando facilitar a vida dos coordenadores dos alunos educadores em escolas profissionalizantes.</p>
													<a href='simplify.php' class='btn btn-primary'>Visitar</a>
												</div>
											</div>
										</div>

										<div class='col-sm'>
											<div class='card ca' style='width: 18rem;'>
													<img class='card-img-top img-fluid' src='imagens/BoliPoint1.svg' alt='Imagem de capa do card'>
												<div class='card-body'>
													<h5 class='card-title'>BoliPoint</h5>
													<p class='card-text'>Projeto criado para quem ama Boliche, visite e conheça mais sobre.</p>
													<a href='BoliPoint.php' class='btn btn-primary'>Visitar</a>
												</div>
											</div>
										</div>

										<div class='col-sm'>
											<div class='card ca' style='width: 18rem;'>
													<img class='card-img-top img-fluid' src='imagens/Fbox_logo.png' alt='Imagem de capa do card'>
												<div class='card-body'>
													<h5 class='card-title'>Próximo projeto, fluxo de caixa. Fbox</h5>
													<p class='card-text'></p>
												</div>
											</div>
										</div>
									</div>								
								</div>		
							";
					echo "
								<footer class='footer container-fluid'>
										<div class='row no-gutters'>
											<div class='fo col-sm'>
												<img class='img-fluid' src='imagens/gmail.png' width='50px' height='50px' />
												<h4>Email</h4>
												<p>jefferson.dev.alu@gmail.com</p>
											</div>
											<div class='fo col-sm'>
												<img class='img-fluid' src='imagens/whatsapp.png' width='50px' height='50px' />
												<h4>Whatsapp</h4>
												<p>+55(85)98884-8945</p>
											</div>
											<div class='fo col-sm'>
												<img class='img-fluid' src='imagens/pin-de-localizacao.png' width='50px' height='50px'/>
												<h4>Localização</h4>
												<p>Brasil, Fortaleza/Ceará</p>

											</div>
										</div>

								</footer>
								

								<script type='text/javascript' src='js/jquery.js'></script>
								<script type='text/javascript' src='js/bootstrap.min.js'></script>
								<script type='text/javascript' src='js/popper.min.js'></script>
								<script type='text/javascript' src='js/Envio.js'></script>
								
			</body>
		</html>
		";
?>